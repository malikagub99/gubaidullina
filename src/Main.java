import exception.UserNotFoundException;
import model.User;
import repo.UserRepositoryImpl;
import service.UserService;
import service.UserServiceImpl;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl(
                new UserRepositoryImpl("userDataBase.txt")
        );

        User user = new User(1, "Igor", 26, true);
        userService.save(user);

        User user1 = new User(2, "Igor1", 16, false);
        userService.save(user1);

        User user2 = new User(4, "Igor4", 22, true);
        userService.save(user2);

        try {
            User update = new User(2, "IgorUpdate", 17, true);
            userService.update(update);

            List<User> userList = userService.findAll();
            for (User u : userList) {
                System.out.println(u);
            }
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }

    }
}
