package service;

import exception.UserNotFoundException;
import model.User;

import java.util.List;

public interface UserService {
    void update(User updateUser) throws UserNotFoundException;
    void save(User user);
    List<User> findAll();
}
