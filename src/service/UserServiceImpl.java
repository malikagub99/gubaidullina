package service;

import exception.UserNotFoundException;
import model.User;
import repo.UserRepository;

import java.util.List;

public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void update(User updateUser) throws UserNotFoundException {
        User user = userRepository.findById(updateUser.getId()).orElseThrow(
                () -> new UserNotFoundException(String.format("Cant find user with id %s", updateUser.getId()))
        );

        user.setName(updateUser.getName());
        user.setAge(updateUser.getAge());

        userRepository.update(user);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
