package repo;

import model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<User> findById(Integer id);
    void update(User user);
    List<User> findAll();
    void save(User user);
}
