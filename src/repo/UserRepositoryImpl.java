package repo;

import model.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {
    private final String fileName;

    public UserRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Optional<User> findById(Integer id) {
        List<User> users = findAll();
        for (User user : users) {
            if (user.getId().equals(id)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    @Override
    public void update(User updateUser) {
        List<User> users = findAll();

        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, false);
            bufferedWriter = new BufferedWriter(writer);

            for (User user: users) {
                if (user.getId().equals(updateUser.getId())) {
                    user.setAge(updateUser.getAge());
                    user.setName(updateUser.getName());
                    user.setWorker(updateUser.getWorker());
                }

                bufferedWriter.write(user.getId() + "|" + user.getName() +
                        "|" + user.getAge() + "|" + user.getWorker());

                bufferedWriter.newLine();
                bufferedWriter.flush();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }

    @Override
    public List<User> findAll() {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)
        ) {
            List<User> users = new ArrayList<>();
            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");

                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);

                users.add(new User(id, name, age, isWorker));
                line = bufferedReader.readLine();
            }
            return users;

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getId() + "|" + user.getName() + "|" +
                    user.getAge() + "|" + user.getWorker());

            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }
}
